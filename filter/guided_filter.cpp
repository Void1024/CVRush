#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
cv::Mat guidedFilter(cv::Mat& _I, cv::Mat& _P, int r, double eps) {
    cv::Mat I, P;
    _I.convertTo(I, CV_32FC1);
    _P.convertTo(P, CV_32FC1);
    cv::Mat mean_P, mean_I, mean_II, mean_IP, var_I, cov_IP, A, B, mean_A, mean_B, Q;
    cv::boxFilter(P, mean_P, -1, cv::Size(r, r));
    cv::boxFilter(I, mean_I, -1, cv::Size(r, r));
    cv::boxFilter(I.mul(I), mean_II, -1, cv::Size(r, r));
    cv::boxFilter(I.mul(P), mean_IP, -1, cv::Size(r, r));
    cv::subtract(mean_II, mean_I.mul(mean_I), var_I);
    cv::subtract(mean_IP, mean_I.mul(mean_P), cov_IP);
    A = cov_IP / (var_I + eps);
    B = mean_P - A.mul(mean_I);
    cv::boxFilter(A, mean_A, -1, cv::Size(r, r));
    cv::boxFilter(B, mean_B, -1, cv::Size(r, r));
    cv::add(mean_A.mul(I), mean_B, Q);
    Q.convertTo(Q, _I.type());
    return Q;
}

int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    std::vector<cv::Mat> channels;
    cv::split(in, channels);
    int r = std::atoi(argv[3]);
    double eps = std::atof(argv[4]);
    for(auto& mat : channels) {
        mat = guidedFilter(mat, mat, r, eps);
    }
    cv::Mat out;
    cv::merge(channels, out);
    cv::imwrite(argv[2], out);
    return 0;
}