#include <iostream>
#include "opencv2/opencv.hpp"
cv::Mat boxFilter(cv::Mat& in, int k_size) {
    const int C = in.channels();
    if (k_size % 2 == 0) {
        ++k_size;
    }
    cv::Mat tem(in.size(), in.type());
    cv::Mat out = tem.clone();
    for (int i = 0; i < in.rows; ++i) {
        uint8_t* p_in = in.ptr<uint8_t>(i);
        uint8_t* p_tem = tem.ptr<uint8_t>(i);
        for (int j = 0; j < in.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                int val = 0;
                for (int x = j - k_size / 2; x < j + k_size / 2 + 1; ++x) {
                    int cur_x = x;
                    if (x < 0) {
                        cur_x *= -1;
                    } else if (x >= in.cols) {
                        cur_x = 2 * in.cols - x - 1;
                    }
                    val += p_in[cur_x * C + c];
                }
                p_tem[j * C + c] = val / k_size;
            }
        }
    }

    for (int i = 0; i < tem.rows; ++i) {
        uint8_t* p_out = out.ptr<uint8_t>(i);
        for (int j = 0; j < tem.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                int val = 0;
                for (int y = i - k_size / 2; y < i + k_size / 2 + 1; ++y) {
                    int cur_y = y;
                    if (y < 0) {
                        cur_y *= -1;
                    } else if (y >= tem.rows) {
                        cur_y = 2 * tem.rows - y - 1;
                    }
                    val += (tem.ptr<uint8_t>(cur_y))[j * C + c];
                }
                p_out[j * C + c] = val / k_size;
            }
        }
    }
    return out;
}

int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    int size = std::atoi(argv[3]);
    cv::Mat out = boxFilter(in, size);
    cv::imwrite(argv[2], out);
    return 0;
}