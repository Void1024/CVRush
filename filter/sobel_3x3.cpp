#include <iostream>
#include "opencv2/opencv.hpp"
cv::Mat Sobel(cv::Mat& in, int dx, int dy) {
    std::vector<int> kernel_hor, kernel_ver;
    if (dx && dy) {
        cv::Mat horizontal = Sobel(in, 1, 0);
        cv::Mat vertical = Sobel(in, 0, 1);
        return horizontal + vertical;
    } else if (dx) {
        kernel_hor = {1, 0, -1};
        kernel_ver = {1, 2, 1};
    } else if (dy) {
        kernel_hor = {1, 2, 1};
        kernel_ver = {1, 0, -1};
    } else {
        return cv::Mat();
    }

    const int C = in.channels();
    const int k_size = 3;
    cv::Mat tem(in.size(), CV_32FC1);
    cv::Mat out = tem.clone();
    for (int i = 0; i < in.rows; ++i) {
        uint8_t* p_in = in.ptr<uint8_t>(i);
        float* p_tem = tem.ptr<float>(i);
        for (int j = 0; j < in.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                int val = 0;
                for (int x = j - k_size / 2, offset = 0; x < j + k_size / 2 + 1;
                     ++x, ++offset) {
                    int cur_x = x;
                    if (x < 0) {
                        cur_x *= -1;
                    } else if (x >= in.cols) {
                        cur_x = 2 * in.cols - x - 1;
                    }
                    val += p_in[cur_x * C + c] * kernel_hor[offset];
                }
                p_tem[j * C + c] = val;
            }
        }
    }

    for (int i = 0; i < tem.rows; ++i) {
        float* p_out = out.ptr<float>(i);
        for (int j = 0; j < tem.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                int val = 0;
                for (int y = i - k_size / 2, offset = 0; y < i + k_size / 2 + 1;
                     ++y, ++offset) {
                    int cur_y = y;
                    if (y < 0) {
                        cur_y *= -1;
                    } else if (y >= tem.rows) {
                        cur_y = 2 * tem.rows - y - 1;
                    }
                    val += (tem.ptr<float>(cur_y))[j * C + c] *
                           kernel_ver[offset];
                }
                p_out[j * C + c] = val;
            }
        }
    }
    return out;
}

int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    cv::cvtColor(in, in, cv::COLOR_BGR2GRAY);
    int dx = std::atoi(argv[3]);
    int dy = std::atoi(argv[4]);
    cv::Mat out = Sobel(in, dx, dy);
    double v_max, v_min;
    cv::minMaxLoc(out, &v_min, &v_max);
    // out = (out - v_min) / (v_max - v_min) * 255.0f;
    out.convertTo(out, CV_8UC1);
    cv::imwrite(argv[2], out);
    return 0;
}