#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
void globalHistogramStatistics(cv::Mat& gray, int& max_v, float& mean,
                               float& std_dev) {
    std::vector<float> hist(256, 0);
    max_v = 0;
    mean = 0.0f;
    std_dev = 0.0f;
    for (int i = 0; i < gray.rows; ++i) {
        uint8_t* p = gray.ptr<uint8_t>(i);
        for (int j = 0; j < gray.cols; ++j) {
            hist[p[j]] += 1.0f / gray.total();
            max_v = max_v > p[j] ? max_v : p[j];
            mean += 1.0f * int(p[j]) / int(gray.total());
        }
    }
    for (int i = 0; i < 256; ++i) {
        std_dev += hist[i] * std::pow(mean - i, 2);
    }
    std_dev = std::sqrt(std_dev);
}
cv::Mat localHistogramEnhancement(cv::Mat& gray, int g_max_v, float g_mean,
                                  float g_std_dev, int window_size, float k0,
                                  float k1, float k2, float k3) {
    if (window_size % 2 == 0) {
        window_size += 1;
    }
    int half = window_size / 2;
    cv::Mat factor = cv::Mat::ones(gray.size(), CV_32FC1);
    for (int i = 0; i < gray.rows; ++i) {
        float* p_factor = factor.ptr<float>(i);
        for (int j = 0; j < gray.cols; ++j) {
            std::vector<float> hist(256, 0);
            int max_v = 0;
            float mean = 0.0f, std_dev = 0.0f;
            for (int y = i - half; y < i + half + 1; ++y) {
                int cur_y;
                if (y < 0) {
                    cur_y = -y;
                } else if (y >= gray.cols) {
                    cur_y = 2 * gray.cols - y - 1;
                } else {
                    cur_y = y;
                }
                uint8_t* p = gray.ptr<uint8_t>(cur_y);
                for (int x = j - half; x < j + half + 1; ++x) {
                    int cur_x;
                    if (x < 0) {
                        cur_x = -x;
                    } else if (x >= gray.rows) {
                        cur_x = 2 * gray.rows - x - 1;
                    } else {
                        cur_x = x;
                    }
                    max_v = max_v > p[cur_x] ? max_v : p[cur_x];
                    hist[p[cur_x]] += 1.0f / (window_size * window_size);
                    mean += int(p[cur_x]) / float(window_size * window_size);
                }
            }
            for (int v = 0; v < 256; ++v) {
                std_dev += hist[v] * std::pow(mean - v, 2);
            }
            std_dev = std::sqrt(std_dev);
            if ((std_dev >= k0 * g_std_dev) && (std_dev <= k1 * g_std_dev) &&
                (mean >= k2 * g_mean) && (mean <= k3 * g_mean)) {
                p_factor[j] = 1.0f * g_max_v / max_v;
            }
        }
    }
    return factor;
}
cv::Mat contrastEnhancement(cv::Mat& img, int window_size, float k0, float k1,
                            float k2, float k3) {
    int g_max_v;
    float g_mean, g_std_dev;
    cv::Mat gray;
    cv::Mat result = img.clone();
    cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);
    globalHistogramStatistics(gray, g_max_v, g_mean, g_std_dev);
    cv::Mat factor = localHistogramEnhancement(gray, g_max_v, g_mean, g_std_dev,
                                               window_size, k0, k1, k2, k3);
    for (int i = 0; i < result.rows; ++i) {
        uint8_t* p = result.ptr<uint8_t>(i);
        float* p_factor = factor.ptr<float>(i);
        for (int j = 0; j < result.cols; ++j) {
            for (int c = 0; c < result.channels(); ++c) {
                p[j * result.channels() + c] *= p_factor[j];
            }
        }
    }
    return result;
}
int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    int window_size = std::atoi(argv[3]);
    float k0 = std::atof(argv[4]);
    float k1 = std::atof(argv[5]);
    float k2 = std::atof(argv[6]);
    float k3 = std::atof(argv[7]);
    cv::Mat out = contrastEnhancement(in, window_size, k0, k1, k2, k3);
    cv::imwrite(argv[2], out);
    return 0;
}