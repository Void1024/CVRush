#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
cv::Mat histogramEqualization(cv::Mat& in, int max_value = 255) {
    int C = in.channels();
    std::vector<std::vector<float>> hists(
            C, std::vector<float>(max_value + 1, 0.0f));

    for (int i = 0; i < in.rows; ++i) {
        uint8_t* p = in.ptr<uint8_t>(i);
        for (int j = 0; j < in.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                hists[c][p[j * C + c]] += 1.0f;
            }
        }
    }

    float total;
    for (auto& hist : hists) {
        for (int i = 1; i < hist.size(); ++i) {
            hist[i] += hist[i - 1];
        }
        total = hist[hist.size() - 1];
        for (int i = 0; i < hist.size(); ++i) {
            hist[i] = round(hist[i] / total * max_value);
        }
    }

    cv::Mat out(in.size(), in.type());
    for (int i = 0; i < out.rows; ++i) {
        uint8_t* p_in = in.ptr<uint8_t>(i);
        uint8_t* p_out = out.ptr<uint8_t>(i);
        for (int j = 0; j < out.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                p_out[j * C + c] =
                        static_cast<uint8_t>(hists[c][p_in[j * C + c]]);
            }
        }
    }
    return out;
}

int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    cv::Mat out = histogramEqualization(in);
    cv::imwrite(argv[2], out);
    return 0;
}