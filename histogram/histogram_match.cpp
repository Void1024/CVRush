#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
cv::Mat histogramEqualization(cv::Mat& in, int max_value = 255) {
    int C = in.channels();
    std::vector<std::vector<float>> hists(
            C, std::vector<float>(max_value + 1, 0.0f));

    for (int i = 0; i < in.rows; ++i) {
        uint8_t* p = in.ptr<uint8_t>(i);
        for (int j = 0; j < in.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                hists[c][p[j * C + c]] += 1.0f;
            }
        }
    }

    float total;
    for (auto& hist : hists) {
        for (int i = 1; i < hist.size(); ++i) {
            hist[i] += hist[i - 1];
        }
        total = hist[hist.size() - 1];
        for (int i = 0; i < hist.size(); ++i) {
            hist[i] = round(hist[i] / total * max_value);
        }
    }

    cv::Mat out(in.size(), in.type());
    for (int i = 0; i < out.rows; ++i) {
        uint8_t* p_in = in.ptr<uint8_t>(i);
        uint8_t* p_out = out.ptr<uint8_t>(i);
        for (int j = 0; j < out.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                p_out[j * C + c] =
                        static_cast<uint8_t>(hists[c][p_in[j * C + c]]);
            }
        }
    }
    return out;
}
cv::Mat histogramMatch(cv::Mat& in, cv::Mat& guide, int max_value = 255) {
    int C = guide.channels();
    std::vector<std::vector<float>> hists(
            C, std::vector<float>(max_value + 1, 0.0f));
    std::vector<std::vector<int>> reverse_hist_map(
            C, std::vector<int>(max_value + 1, -1));

    cv::Mat equalized_in = histogramEqualization(in, max_value);

    for (int i = 0; i < guide.rows; ++i) {
        uint8_t* p = guide.ptr<uint8_t>(i);
        for (int j = 0; j < guide.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                hists[c][p[j * C + c]] += 1.0f;
            }
        }
    }

    float total;
    for (int c = 0; c < C; ++c) {
        auto& hist = hists[c];
        for (int i = 1; i < hist.size(); ++i) {
            hist[i] += hist[i - 1];
        }
        total = hist[hist.size() - 1];
        for (int i = 0; i < hist.size(); ++i) {
            hist[i] = round(hist[i] / total * max_value);
            if (reverse_hist_map[c][hist[i]] == -1) {
                reverse_hist_map[c][hist[i]] = i;
            }
        }
    }

    for (int i = 0; i < equalized_in.rows; ++i) {
        uint8_t* p = equalized_in.ptr<uint8_t>(i);
        for (int j = 0; j < equalized_in.cols; ++j) {
            for (int c = 0; c < C; ++c) {
                p[j * C + c] = reverse_hist_map[c][p[j * C + c]];
            }
        }
    }

    return equalized_in;
}
int main(int argc, char** argv) {
    cv::Mat in = cv::imread(argv[1]);
    cv::Mat guide = cv::imread(argv[2]);
    cv::Mat out = histogramMatch(in, guide);
    cv::imwrite(argv[3], out);
    return 0;
}