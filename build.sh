#! /bin/bash
echo "BUILD TARGET:" $1
echo "BUILDING..."
clang++ $1 -o run `pkg-config --cflags --libs opencv`
echo "BUILD FINISH!"